{!! $headerandfooter['header_top'] !!}
	@yield('style')
</head>

{!! $headerandfooter['header_lower'] !!}

@yield('content')

{!! $headerandfooter['footer'] !!}

<script type="text/javascript">
    var base_url = '{{ URL::to("/") }}';
</script>

@yield('scripts')
<script>
	(function ($) {
		$("#options-tv-show .radio-option").click(function (evt) {
			evt.preventDefault();
			
			if($(this).attr('class') == 'radio-option active'){
				$('#options-tv-show .radio-option').removeClass('active');
				$(this).parent().find('input').prop('checked', false);
			} else {
				$('#options-tv-show .radio-option').removeClass('active');
				$(this).addClass('active');
				$(this).parent().find('input').click();
			}
	    });
	    $("#options-obstacle .radio-option").click(function (evt) {
			evt.preventDefault();
			
			if($(this).attr('class') == 'radio-option active'){
				$('#options-obstacle .radio-option').removeClass('active');
				$(this).parent().find('input').prop('checked', false);
			} else {
				$('#options-obstacle .radio-option').removeClass('active');
				$(this).addClass('active');
				$(this).parent().find('input').click();
			}
	    });
	    $("#options-animal .radio-option").click(function (evt) {
			evt.preventDefault();

			if($(this).attr('class') == 'radio-option active'){
				$('#options-animal .radio-option').removeClass('active');
				$(this).parent().find('input').prop('checked', false);
			} else {
				$('#options-animal .radio-option').removeClass('active');
				$(this).addClass('active');
				$(this).parent().find('input').click();
			}
	    });
	})(jQuery);
</script>
</body>
</html>