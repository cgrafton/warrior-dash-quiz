@extends('layouts.master')
@section('content')
<div id="page-header" style="background-image:url(https://s3.amazonaws.com/warriordash/app/uploads/2013/06/banner-warriornation.jpg);">
	<div id="page-title">
		<h1>What kind of Warrior are you?</h1>
	</div>
</div>
<div class="container">

    <div class="row text-center">
        <div class="col-md-10">
           <p>Whether you’re prepping for your first Warrior Dash, or planning to repeat your favorite obstacles year after year—take this quiz to determine the Warrior you are, on and off the course. Share the results with your friends before bringing your Warrior-self to conquer the Battleground!</p>

        </div>
    </div>

    {!! Form::open(array('url' => 'submit', 'files' => true, 'class' => 'which_warrior', 'id' => 'which_warrior')) !!}
    {{--if quiz has been taken display warrior type description and image--}}
    @if ($headerandfooter['message'])
    {!! $headerandfooter['message'] !!}
    <img src={{asset($headerandfooter['image'])}} alt="">
    @endif

    {{--Go To Workout--}}
    <div class="row">
        <div class="col-md-5">
            {!! Form::label('go_to_workout', 'What\'s your go-to workout?*') !!}
            <div class="error go_to_workout">{{ $errors->first('go_to_workout') }}</div>
            {!! Form::select('go_to_workout', array
            ('' => '',
            '7' => 'Chasing the nicest butt',
            '1'  => 'Anything to help me achieve my personal goals',
            '6'  => 'Walking or jogging for a good cause',
            '4'  => 'Crossfit and interval training',
            '5'  => 'Group classes or team sports',
            '3'  => 'Warrior Dash, another 5K, half marathons, triathlons...',
            '2'  => 'Perfecting my beer pong shot'
            ), '') !!}
        </div>

        {{--Ultimate Pump Up Song--}}
        <div class="col-md-5 col-md-offset-1">
            {!! Form::label('workout_song', 'What song title best describes your workout style?*') !!}
            <div class="error workout_song">{{ $errors->first('workout_song') }}</div>
            {!! Form::select('workout_song', array
            ('' => '',
            '7' => 'Call Me Maybe (Carly Rae Jepsen)',
            '1'  => 'I Believe I Can Fly (R. Kelly)',
            '5'  => 'This Is How We Roll (Florida Georgia Line)',
            '6'  => 'Can\'t Hold Us (Macklemore & Ryan Lewis)',
            '2'  => 'Rock And Roll All Nite (KISS)',
            '3'  => 'Stronger (Kayne West)',
            '4'  => 'Nothin\' but a Good Time (Poison)'
            ), '') !!}
        </div>
        {{--Beverage--}}
        <div class="col-md-5">
            {!! Form::label('beverage', 'What\'s your favorite post-race beverage?*') !!}
            <div class="error beverage">{{ $errors->first('beverage') }}</div>
            {!! Form::select('beverage', array
            ('' => '',
            '6' => 'Any drink I can grab at the St Jude tent',
            '4'  => 'I just want to drink my free beer',
            '5'  => 'Whatever my friends are having',
            '3'  => 'Sports endurance drinks, like Gatorade or Powerade',
            '7'  => 'Nothing -- I am just here to mingle',
            '2'  => 'An ice cold Shock Top, or three',
            '4'  => 'Water, lots of water'
            ), '') !!}
        </div>
        
        {{--Catchphrase--}}
        <div class="col-md-5 col-md-offset-1">
            {!! Form::label('catchphrase', 'If you had a catchphrase, it would be:*') !!}
            <div class="error catchphrase">{{ $errors->first('catchphrase') }}</div>
            {!! Form::select('catchphrase', array
            ('' => '',
            '7' => '“Everything happens for a reason.”',
            '4'  => '“Eat, drink, and be merry.”',
            '1'  => '“Every day is a second chance.”',
            '3'  => '“Can’t stop, won’t stop.”',
            '5'  => '“Teamwork makes the dream work.”',
            '6'  => '“Giving back is always in style.”',
            '2'  => '“The party don’t start ‘til I walk in.”'
            ), '') !!}
        </div>

        {{--Ultimate Workout Partner--}}
        <div class="col-md-12">
            {!! Form::label('workout_partner', 'Pick your ultimate workout partner.*') !!}
            <div class="error workout_partner">{{ $errors->first('workout_partner') }}</div>
            <div class="radio-row">
                <div class="radio-block">
                    {!! Form::radio('workout_partner', '5') !!}
                    {!! Form::label('workout_partner', "Zach Galifianakis") !!}
                </div>
                <div class="radio-block">
                    {!! Form::radio('workout_partner', '4') !!}
                    {!! Form::label('workout_partner', 'Jennifer Lawrence') !!}
                </div>
                <div class="radio-block">
                    {!! Form::radio('workout_partner', '7') !!}
                    {!! Form::label('workout_partner', 'Ryan Gosling') !!}
                </div>
                <div class="radio-block">
                    {!! Form::radio('workout_partner', '6') !!}
                    {!! Form::label('workout_partner', 'Bono') !!}
                </div>
                <div class="radio-block">
                    {!! Form::radio('workout_partner', '1') !!}
                    {!! Form::label('workout_partner', 'Jillian Michaels') !!}
                </div>
                <div class="radio-block">
                    {!! Form::radio('workout_partner', '3') !!}
                    {!! Form::label('workout_partner', 'Michael Phelps') !!}
                </div>
                <div class="radio-block">
                    {!! Form::radio('workout_partner', '2') !!}
                    {!! Form::label('workout_partner', 'Miley Cyrus') !!}
                </div>
            </div>
        </div>
        
        {{--Tv Show--}}
        <div class="col-md-12">
            {!! Form::label('tv_show', 'If your life were a TV show, which one would it be?*') !!}
            <div class="error tv_show">{{ $errors->first('tv_show') }}</div>
            <div class="radio-row" id="options-tv-show">
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/tv-shows/snl.jpg')}} alt="">
                	</div>
                    {!! Form::radio('tv_show', '2') !!}
                    {!! Form::label('tv_show', 'Saturday Night Live') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/tv-shows/friends.jpg')}} alt="">
                	</div>
                    {!! Form::radio('tv_show', '5') !!}
                    {!! Form::label('tv_show', 'Friends') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/tv-shows/big-bang.jpg')}} alt="">
                	</div>
                    {!! Form::radio('tv_show', '4') !!}
                    {!! Form::label('tv_show', 'The Big Bang Theory') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/tv-shows/once-upon.jpg')}} alt="">
                	</div>
                    {!! Form::radio('tv_show', '7') !!}
                    {!! Form::label('tv_show', 'Once upon a Time') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/tv-shows/game.jpg')}} alt="">
                	</div>
                    {!! Form::radio('tv_show', '3') !!}
                    {!! Form::label('tv_show', 'Game of Thrones') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/tv-shows/modern-family.jpg')}} alt="">
                	</div>
                    {!! Form::radio('tv_show', '6') !!}
                    {!! Form::label('tv_show', 'Modern Family') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/tv-shows/biggest.jpg')}} alt="">
                	</div>
                    {!! Form::radio('tv_show', '1') !!}
                    {!! Form::label('tv_show', 'The Biggest Loser') !!}
                </div>
            </div>
        </div>

        {{--Favorite Warrior Dash Obstacle--}}
        <div class="col-md-12">
            {!! Form::label('favorite_obstacle', 'Pick your favorite Warrior Dash obstacle.*') !!}
            <div class="error favorite_obstacle">{{ $errors->first('favorite_obstacle') }}</div>
            <div class="radio-row" id="options-obstacle">
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/obstacles/goliath.jpg')}} alt="">
                	</div>
                    {!! Form::radio('favorite_obstacle', '4') !!}
                    {!! Form::label('favorite_obstacle', 'Goliath') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/obstacles/warrior-wall.jpg')}} alt="">
                	</div>
                    {!! Form::radio('favorite_obstacle', '3') !!}
                    {!! Form::label('favorite_obstacle', 'Great Warrior Wall') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/obstacles/mud-mounds.jpg')}} alt="">
                	</div>
                    {!! Form::radio('favorite_obstacle', '5') !!}
                    {!! Form::label('favorite_obstacle', 'Mud Mounds') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/obstacles/high-tension.jpg')}} alt="">
                	</div>
                    {!! Form::radio('favorite_obstacle', '6') !!}
                    {!! Form::label('favorite_obstacle', 'High Tension') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/obstacles/diesel-dome.jpg')}} alt="">
                	</div>
                    {!! Form::radio('favorite_obstacle', '7') !!}
                    {!! Form::label('favorite_obstacle', 'Diesel Dome') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/obstacles/chaotic-cargo.jpg')}} alt="">
                	</div>
                    {!! Form::radio('favorite_obstacle', '1') !!}
                    {!! Form::label('favorite_obstacle', 'Chaotic Cargo') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/obstacles/muddy-mayhem.jpg')}} alt="">
                	</div>
                    {!! Form::radio('favorite_obstacle', '2') !!}
                    {!! Form::label('favorite_obstacle', 'Muddy Mayhem') !!}
                </div>
            </div>
        </div>


        {{--Spirit Animal--}}
        <div class="col-md-12">
            {!! Form::label('spirit_animal', 'What\'s your spirit animal?*') !!}
            <div class="error spirit_animal">{{ $errors->first('spirit_animal') }}</div>
            <div class="radio-row" id="options-animal">
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/animal/peacock.jpg')}} alt="">
                	</div>
                    {!! Form::radio('spirit_animal', '2') !!}
                    {!! Form::label('spirit_animal', 'Peacock') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/animal/cheetah.jpg')}} alt="">
                	</div>
                    {!! Form::radio('spirit_animal', '3') !!}
                    {!! Form::label('spirit_animal', 'Cheetah') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/animal/deer.jpg')}} alt="">
                	</div>
                    {!! Form::radio('spirit_animal', '6') !!}
                    {!! Form::label('spirit_animal', 'Deer') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/animal/wolf.jpg')}} alt="">
                	</div>
                    {!! Form::radio('spirit_animal', '5') !!}
                    {!! Form::label('spirit_animal', 'Wolf') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/animal/butterfly.jpg')}} alt="">
                	</div>
                    {!! Form::radio('spirit_animal', '1') !!}
                    {!! Form::label('spirit_animal', 'Butterfly') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/animal/dove.jpg')}} alt="">
                	</div>
                    {!! Form::radio('spirit_animal', '7') !!}
                    {!! Form::label('spirit_animal', 'Dove') !!}
                </div>
                <div class="radio-block">
                	<div class="radio-option">
                		<img src={{asset('img/animal/dolphin.jpg')}} alt="">
                	</div>
                    {!! Form::radio('spirit_animal', '4') !!}
                    {!! Form::label('spirit_animal', 'Dolphin') !!}
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    <div class="row text-center">
        {!! Form::submit('Submit', array('class' => 'button')) !!}
        <br/>
        <br/>
    </div>
    {!! Form::close() !!}
    
	<div id="result-lightbox" class="hidden">
		<a href="https://www.facebook.com/dialog/feed?app_id=694165360698061&link=http%3A%2F%2Flocalhost%2Fwarrior-dash-quiz%2Fpublic%2F&picture=http%3A%2F%2Flocalhost%2Fwarrior-dash-quiz%2Fpublic%2Fimage.jpg&name=What%20kind%20of%20Warrior%20are%20you%3F&caption=%20&description=I'm%20a%20blank%20Warrior&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F" target="_blank">Share on Facebook</a>
		
		<a href="" target="_blank">Share on Twitter</a>
	</div><!--/result-lightbox-->
	
</div>
</div>
@stop

@section('style')
<link rel='stylesheet' href='css/styles.css' type='text/css'/>
@stop


