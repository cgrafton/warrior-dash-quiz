<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'WarriorDashQuiz@loadQuiz');
Route::post('submit', 'WarriorDashQuiz@validateResults');

//post survey close survey redirect to home page
//Route::get('/', function()
//{
//    return Redirect::away('http://warriordash.com');
//});
