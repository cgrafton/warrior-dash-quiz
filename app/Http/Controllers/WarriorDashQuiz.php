<?php namespace WarriorDashQuiz\Http\Controllers;

use Session;
use Validator;
use Redirect;
use WarriorDashQuiz\Entities\WarriorTypes\Repositories\EloquentWarriorTypeRepository;


class WarriorDashQuiz extends Controller
{

    protected $warriorTypeRepository;

    public function __construct()
    {
        $this->warriorTypeRepository = new EloquentWarriorTypeRepository();
    }

    public function loadQuiz()
    {

        //grab header and footer
        $header = file_get_contents('http://warriordash.com/header/');
        //Replace Title of Page
        $startPoint = '<title>';
        $endPoint = '</title>';
        $pagetitle = "Warrior Dash";
        $header = preg_replace('#(' . preg_quote($startPoint) . ')(.*)(' . preg_quote($endPoint) . ')#si',
            '$1' . $pagetitle . '$3', $header);
        $footer = file_get_contents('http://warriordash.com/footer/');
        $header = explode('</head>', $header);
        $header_and_footer = [
            'header_top'   => $header[0],
            'header_lower' => $header[1],
            'footer'       => $footer,
            'message'      => '',
            'image'        => ''
        ];
        //store in session
        Session::put('headerandfooter', $header_and_footer);

        //redirect to view
        return view('warriordashquiz', ['headerandfooter' => $header_and_footer]);
    }

    public function validateResults()
    {
        //get data
        $input = \Input::all();
        //run validation
        $validator = Validator::make($input, [
            'go_to_workout'     => 'required',
            'workout_song'      => 'required',
            'tv_show'           => 'required',
            'beverage'          => 'required',
            'workout_partner'   => 'required',
            'favorite_obstacle' => 'required',
            'catchphrase'       => 'required',
            'spirit_animal'     => 'required',
            'spirit_animal'     => 'required'
        ]);
        //redirect with errors if they exist
        if ($validator->fails()) {
            return redirect::to('/')->withErrors($validator);
        }
        Session::put('quiz_results', $input);

        //calculate warrior type (returns a redirec to view)
        return $this->calculateWarriorType();

    }

    public function calculateWarriorType()
    {
        //grab header and footer for redirect
        $header_and_footer = Session::get('headerandfooter');
        //grab quiz results
        $quiz_results = Session::get('quiz_results');

        //store array of warrior_type_id-> count of votes for warrior_type
        $quanity_of_answers_for_each_warrior_type_id = array_count_values($quiz_results);

        //TO DO put a check if empty or not an array

        //sort from highest to lowest
        arsort($quanity_of_answers_for_each_warrior_type_id);

        //save sorted array because we will be taking first element out
        $highest_warrior_type_counts = $quanity_of_answers_for_each_warrior_type_id;

        //grab the warrior_id with the highest quantity
        $highest_quanity_of_answers_warrior_type_id = array_shift($quanity_of_answers_for_each_warrior_type_id);

        //check to see if any other warrior_types has the same count as the highest. store all ties in an array
        foreach ($highest_warrior_type_counts as $quanity_of_answers_for_warrior_type) {
            if ($highest_quanity_of_answers_warrior_type_id == $quanity_of_answers_for_warrior_type) {
                $highest_qanitity_of_warrior_types_ids = array_keys($highest_warrior_type_counts,
                    $quanity_of_answers_for_warrior_type);
            }
        }
        //randomly pick warrior type
        shuffle($highest_qanitity_of_warrior_types_ids);
        $warrior_type_id = array_shift($highest_qanitity_of_warrior_types_ids);

        //get title of type and description and photo from id
        $warrior_type_title = $this->warriorTypeRepository->getWarriorTypeTitleFromId($warrior_type_id);
        $warrior_type_description = $this->warriorTypeRepository->getWarriorTypeDescriptionFromId($warrior_type_id);

        switch ($warrior_type_id) {
            case 6:
                $header_and_footer['image'] = 'img/warrior-types/big_hearted.jpg';
                break;
            case 3:
                $header_and_footer['image'] = 'img/warrior-types/iron.jpg';
                break;
            case 7:
                $header_and_footer['image'] = 'img/warrior-types/star_crossed.jpg';
                break;
            case 4:
                $header_and_footer['image'] = 'img/warrior-types/weekend_warrior.jpg';
                break;
            case 2:
                $header_and_footer['image'] = 'img/warrior-types/wild_child.jpg';
                break;
            case 5:
                $header_and_footer['image'] = 'img/warrior-types/wolfpack.jpg';
            case 1:
                $header_and_footer['image'] = 'img/warrior-types/wow_factor.jpg';
                break;
        }

        //add count for warrior type chosen to db

        $this->warriorTypeRepository->incrementWarriorTypeQuantity($warrior_type_id);
        //build view data (header and footer and message)

        $header_and_footer['message'] = 'Congrats! You are a ' . $warrior_type_title . '. ' . $warrior_type_description;

        //return view
        return view('warriordashquiz', ['headerandfooter' => $header_and_footer]);

    }


}
