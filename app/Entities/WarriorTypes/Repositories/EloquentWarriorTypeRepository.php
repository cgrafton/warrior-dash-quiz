<?php namespace WarriorDashQuiz\Entities\WarriorTypes\Repositories;


use WarriorDashQuiz\Entities\WarriorTypes\Models\EloquentWarriorType;

class EloquentWarriorTypeRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new EloquentWarriorType();
    }

    public function getWarriorTypeTitleFromId($warrior_type_id)
    {
        $warrior_type = $this->model->where('id', '=', $warrior_type_id)->first(['title']);
        return $warrior_type->title;


    }

    public function getWarriorTypeDescriptionFromId($warrior_type_id)
    {
        $warrior_type = $this->model->where('id', '=', $warrior_type_id)->first(['description']);
        return $warrior_type->description;


    }

    public function incrementWarriorTypeQuantity($warrior_type_id)
    {
        $warrior_type = $this->model->find($warrior_type_id);
        $warrior_type->increment('quantity');
    }
}